package ntu.csie.oop13spring;
public class EricCoordinate extends POOCoordinate{
	public static final int bound = 10;
	public EricCoordinate(){
		x = 0;
		y = 0;
	}

	public EricCoordinate(int _x, int _y){
		x = _x;
		y = _y;
	}

	public boolean equals(POOCoordinate other){
		if(other==null) return false;
		else if(other.x == this.x && other.y == this.y) return true;
		else return false;
	}
	public boolean attack(EricCoordinate other){
		if( Math.abs(other.x - this.x) + Math.abs(other.y - this.y)<=1 )
			return true;
		return false;
	}
	public boolean setCoordinate(int _x, int _y){
		if(_x<0 || _x>=bound || _y<0 || _y>=bound){//out of bound
			return false;
		}
		else{
			x = _x;
			y = _y;
		}
		return true;
	}
}
