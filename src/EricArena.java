package ntu.csie.oop13spring;
import java.util.Scanner;
public class EricArena extends POOArena{
	public boolean fight(){
		int i;
		POOPet[] pet = getAllPets();
		Scanner keyboard = new Scanner(System.in);
		for(i=0;i<pet.length;i++){
			System.out.println("====No."+i+" "+pet[i].getName());
			System.out.print("Which action you want to do?(1.move  2.attack): ");
			int choice = keyboard.nextInt();
			while(choice!=1 && choice!=2){
				System.out.println("You must input 1 or 2!");
				System.out.print("Which action you want to do?(1.move  2.attack): ");
				choice = keyboard.nextInt();
			}
			if(choice == 1){//move
				POOCoordinate coordinate = pet[i].move(this);
				pet[i].setMP(pet[i].getMP()+5);//every moving increases 5 MP
				System.out.println(pet[i].getName()+" increase 5 MP, total MP is "+pet[i].getMP()+" now.");
				System.out.println("Current coordinate: ("+coordinate.x+", "+coordinate.y+")");
			}
			else if(choice == 2){//attack
				POOAction action = pet[i].act(this);
				if(action==null) return false;
				action.skill.act(action.dest);
				if(action.dest.getHP()==0){//game over
					System.out.println(action.dest.getName()+" die!");
					System.out.println("Game over!");
					return false;
				}
			}
		}
		/*for(i=0;i<pet.length;i++){//check whether pets die or not
			if(pet[i].getHP()==0){
				allpets.remove(i);
				System.out.println(pet[i].getName()+" die!");
			}
		}
		if(pet.length==0) return false;*/
		return true;
	}

	public void show(){
		POOPet[] pet = getAllPets();
		for(int i=0;i<pet.length;i++){
			System.out.println("Name: "+pet[i].getName());
			System.out.println("HP: "+pet[i].getHP());
			System.out.println("MP: "+pet[i].getMP());
			System.out.println("AGI: "+pet[i].getAGI());
			POOCoordinate c = getPosition(pet[i]);
			System.out.println("Coordinate: ("+c.x+", "+c.y+")");
			System.out.println();
		}
	}

	public POOCoordinate getPosition(POOPet p){
		if(p instanceof EricPet1){
			EricPet1 myPet = (EricPet1)p;
			return myPet.coordinate;
		}
		return null;
	}
}
