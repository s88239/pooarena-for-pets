package ntu.csie.oop13spring;
import java.util.Scanner;
import java.util.Random;
public class EricPet2 extends EricPet1{
	public EricPet2(){
		super("Bumbler");
		Random rand = new Random();
		action = new POOAction();
		coordinate = new EricCoordinate(9,9);
		setHP(rand.nextInt(100)+150);//HP from 150 to 250
		setMP(rand.nextInt(50)+50);//MP from 50 to 100
		setAGI(rand.nextInt(3)+2);//AGI from 1 to 5
		System.out.println("Name: "+getName());
		System.out.println("HP: "+getHP());
		System.out.println("MP: "+getMP());
		System.out.println("AGI: "+getAGI());
		System.out.println("Coordinate: ("+coordinate.x+", "+coordinate.y+")");
		System.out.println();
	}
}
