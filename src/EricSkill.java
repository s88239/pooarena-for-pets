package ntu.csie.oop13spring;
import java.util.Scanner;
public class EricSkill extends POOSkill{
	public void act(POOPet pet){
		System.out.print("Which ability you want to enhance?(1.HP  2.MP  3.AGI  Other.none): ");
		Scanner keyboard = new Scanner(System.in);
		int choice = keyboard.nextInt();
		int hp, mp, AGI;
		hp = pet.getHP();
		mp = pet.getMP();
		if(choice==1){
			int consumeMP = mp>= 30 ? 30 : mp;
			int increaseHP = mp>=30 ? 100 : 10*mp/3;
			pet.setHP(hp+increaseHP);
			pet.setMP(mp - consumeMP);
			System.out.println(pet.getName()+" increases "+increaseHP+", and you consume "+consumeMP+" MP.");
		}
		else if(choice==2){
			pet.setMP(mp+20);
			System.out.println(pet.getName()+" increases 20 MP, totall MP is "+(mp+20)+" now.");
		}
		else if(choice==3){
			AGI = pet.getAGI();
			pet.setAGI(AGI+1);
			System.out.println(pet.getName()+" increases 1 AGI, the AGI is "+(AGI+1)+" now.");
		}
	}
}

class GoldFinger extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		double attackRatio = Math.random();
		if(attackRatio >= 0.5) System.out.print("Strong attack with ");
		System.out.println("Gold Finger!");
		int loss =(int) ((1+attackRatio)*20);
		hp = hp > loss ? hp - loss : 0;
		pet.setHP(hp);
		System.out.println(pet.getName()+" loses "+loss+" HP, remaining HP is "+hp+" now.");
	}
}

class ShakeHands extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		double attackRatio = Math.random();
		if(attackRatio >= 0.5) System.out.print("Strong attack with ");
		System.out.println("Shake Hands!");
		int loss = (int)((1+attackRatio)*50);
		hp = hp > loss ? hp - loss : 0;
		pet.setHP(hp);
		System.out.println(pet.getName()+" loses "+loss+" HP, remaining HP is "+hp+" now.");
	}
}

class DeclareWar extends POOSkill{
	public void act(POOPet pet){
		int hp = pet.getHP();
		double attackRatio = Math.random();
		if(attackRatio >= 0.5) System.out.print("Strong attack with ");
		System.out.println("Declaration of War to Philippines!");
		int loss = (int)((1+attackRatio)*200);
		hp = hp > loss ? hp - loss : 0;
		pet.setHP(hp);
		System.out.println(pet.getName()+" loses "+loss+" HP, remaining HP is "+hp+" now.");
	}
}
