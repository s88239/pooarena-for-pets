package ntu.csie.oop13spring;
import java.util.Scanner;
import java.util.Random;
public class EricPet1 extends POOPet{
	public POOAction action;
	public EricCoordinate coordinate;
	public EricPet1(){
		Random rand = new Random();
		action = new POOAction();
		coordinate = new EricCoordinate(0,0);
		setName("Ma");
		setHP(rand.nextInt(100)+150);//HP from 150 to 250
		setMP(rand.nextInt(50)+50);//MP from 50 to 100
		setAGI(rand.nextInt(3)+2);//AGI from 1 to 5
		System.out.println("Name: "+getName());
		System.out.println("HP: "+getHP());
		System.out.println("MP: "+getMP());
		System.out.println("AGI: "+getAGI());
		System.out.println("Coordinate: ("+coordinate.x+", "+coordinate.y+")");
		System.out.println();
	}
	public EricPet1(String name){
		setName(name);
	}
	public POOAction act(POOArena arena){
		Scanner keyboard = new Scanner(System.in);
		POOPet[] pet = arena.getAllPets();
		for(int i=0;i<pet.length;i++){//find the ajacent pet
			if(coordinate.attack(((EricPet1)pet[i]).coordinate)){
				System.out.print(i+"."+pet[i].getName()+" ");
			}
		}
		System.out.print("\nPlease choose the number of the targe you want to attack: ");
		int tar = keyboard.nextInt();
		while(tar<0 || tar>=pet.length || !coordinate.attack(((EricPet1)pet[tar]).coordinate)){
			if(tar<0 || tar>=pet.length){
				System.out.println("Can't find the pet of number "+ tar +"!");
			}
			else{
				System.out.println("You can't attack the pet of number"+ tar +"!");
				System.out.println("You can only attack the pet on the adjacent coordinate or yourself!");
			}
			System.out.print("Please input the number of the targe you want to attack: ");
			tar = keyboard.nextInt();
		}
		action.dest = pet[tar];
		System.out.println("1.Gold Finger attack(consume 10 MP)");
		System.out.println("2.Shake Hands with bumbler(consume 25 MP)");
		System.out.println("3.Declaration of War to Philippines(consume 100 MP)");
		System.out.println("4.Enhance the ablity");
		System.out.println("other.Normal Attack");
		System.out.print("Which skill you want to use? ");
		int skillChoose = keyboard.nextInt();
		int myMP = getMP();
		if(skillChoose==1){//use Gold Finger
			if(myMP>=10){
				action.skill = new GoldFinger();
				setMP(myMP - 10);
			}
			else{
				ShortMP("Gold Finger");
			}
		}
		else if(skillChoose==2){//use Shake hands
			if(myMP >= 25){
				action.skill = new ShakeHands();
				setMP(myMP - 25);
			}
			else{
				ShortMP("Shake Hands");
			}
		}
		else if(skillChoose==3){//use declaration of war
			if(myMP >= 100){
				action.skill = new DeclareWar();
				setMP(myMP - 100);
			}
			else{
				ShortMP("Declaration of War");
			}
		}
		else if(skillChoose==4){//enhance the ablity
			action.skill = new EricSkill();
		}
		else{//use general tiny attack
			action.skill = new POOTinyAttackSkill();
			System.out.println(pet[tar].getName()+" loses 1 HP, remaining HP is "+(pet[tar].getHP()-1)+" now.");
		}
		return action;
	}

	public POOCoordinate move(POOArena arena){
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Horizontal steps you want to move (positive for right, negative for left): ");
		int moveX = keyboard.nextInt();
		System.out.print("Vertical steps you want to move (positive for up, negative for down): ");
		int moveY = keyboard.nextInt();
		while( Math.abs(moveX) + Math.abs(moveY) > getAGI() || !coordinate.setCoordinate(coordinate.x+moveX, coordinate.y+moveY)){
			if( Math.abs(moveX) + Math.abs(moveY) > getAGI() ){
				System.out.println("You can't go so far, because your AGI is only "+getAGI()+".");
				System.out.println("Your horizontal steps and vertical steps must add up to less than "+getAGI()+".");
			}
			else{
				System.out.println("Out of bound!");
			}
			System.out.print("Please input horizontal steps again: ");
			moveX = keyboard.nextInt();
			System.out.print("Please input vertical steps again: ");
			moveY = keyboard.nextInt();
		}
		return coordinate;
	}

	public void ShortMP(String str){
		System.out.println("Your MP is "+ getMP() +" now, and it's not enough to use "+str+"!");
		System.out.println("Change to use general tiny attack.");
		action.skill = new POOTinyAttackSkill();
		System.out.println(action.dest.getName()+" loses 1 HP, remaining HP is "+(action.dest.getHP()-1)+" now.");
	}
}
